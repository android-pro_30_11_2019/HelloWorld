package com.facebook.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.e(TAG,"item = "+item.toString());
        switch (item.getItemId()){
            case R.id.setting:
                Toast.makeText(this, getResources().getString(R.string.optionSettingMsg), Toast.LENGTH_SHORT).show();
                break;
            case R.id.logout:
                Toast.makeText(this, getResources().getString(R.string.optionLogoutMsg), Toast.LENGTH_SHORT).show();
                break;
            default: super.onOptionsItemSelected(item);
        }
        return true;
    }
}
